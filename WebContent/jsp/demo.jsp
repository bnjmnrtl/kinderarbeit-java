<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>

</head>

<body>

	<div class="container">
    <div class="row">
    	<h2>Privalino Demo</h2>

		
			<div class="col-md-5">

				<h3>Your input</h3>

				                 <form action="demo" method="post" >
                            <div class="form-group">
                                <textarea
                                    rows="5" class="form-control" name="inputText" id="inputText"
                                    placeholder="Write some text and let Privalino analyze you!" required="required">${inputText}</textarea>
                                    
                            </div>
                            
                            <button type="submit" style="width: 100%; font-size:1.1em;" class="btn btn-large btn btn-success btn-lg btn-block" ><b><span class="glyphicon glyphicon-cog"></span> Analyze</b></button>
                                                   
                        </form>

			</div>

			<div class="col-md-3">

				<!--Sectrion Entry List-->
				<h3>Results</h3>
 <form class="form-horizontal" role="form">

				 <div class="form-group">
<label for="topic" class="col-sm-4 control-label">Age:</label>
        <div class="col-sm-8">
        	<label for="topic" class="col-sm-12 control-label">${age}</label>      
</div>
</div>

				 <div class="form-group">
<label for="topic" class="col-sm-4 control-label">Gender:</label>
        <div class="col-sm-8">
        	<label for="topic" class="col-sm-12 control-label">${gender}</label>      
</div>
</div>
</br>
<img src="privalino.png" class="img-responsive" alt="Privalino">

</div>
		

			<div class="col-md-4">
				<h3>Features</h3>
				<input type="hidden" id="idFeature" name="idFeature">
					<c:choose>
						<c:when test="${not empty features}">
							<table class="table table-striped">
								<thead>
									<tr>
										<td>Feature</td>
										<td>Value</td>
									</tr>
								</thead>
								<c:forEach var="feature" items="${features}">
									<c:set var="classSucess" value="" />
									<c:if test="${idFeature == feature.id}">
										<c:set var="classSucess" value="info" />
									</c:if>
									<tr class="${classSucess}">
										<td>${feature.id}</td>
										<td>${feature.value}</td>
									</tr>
								</c:forEach>
							</table>
						</c:when>
						<c:otherwise>
							<br>
							<div class="alert alert-info">No features defined</div>
						</c:otherwise>
					</c:choose>
				</form>

			</div>
		</div>
	</div>
</body>
</html>