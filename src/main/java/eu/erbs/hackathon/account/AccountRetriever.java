package eu.erbs.hackathon.account;

import eu.erbs.hackathon.NicosTest;
import io.swagger.client.ApiException;

public class AccountRetriever {

	public static void main(String[] args) throws ApiException, Exception {
		String balance = NicosTest.getAccountBalance();
    	System.out.println(balance);

    	for(String transaction : NicosTest.getTransactions()){
    		System.out.println(transaction);
    	}

	}

}
