package eu.erbs.hackathon.account;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.erbs.hackathon.NicosTest;
import eu.erbs.hackathon.ahoi.TransactionFaker;
import io.swagger.client.ApiException;

/**
 * Servlet implementation class AccountNotifier
 */
@WebServlet("/transactions")
public class TransactionNotifier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = LoggerFactory.getLogger(TransactionNotifier.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionNotifier() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		logger.info("Received Get Request");
		for(Entry<String, String[]> entry : request.getParameterMap().entrySet()){
			System.out.println("\t" + entry.getKey() + "\t" + entry.getValue()[0]);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Received Post Request");
		for(Entry<String, String[]> entry : request.getParameterMap().entrySet()){
			System.out.println("\t" + entry.getKey() + "\t" + entry.getValue()[0]);
		}
		
		response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
		response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
//	    try {
//	    	logger.info("Trying to get transactions");
//	    	StringBuffer transactions = new StringBuffer();
//	    	for(String transaction: NicosTest.getTransactions()){
//	    		transactions.append(transaction);
//	    		transactions.append("@@@");
//	    	}
//	    	logger.info(transactions.toString());
//			response.getWriter().write(transactions.toString());
//		} catch (ApiException e ) {
//			throw new ServletException(e);
//		} catch (Exception e) {
//			throw new ServletException(e);
//		}
	    response.getWriter().write(TransactionFaker.getTransactionsAsString());
	    response.addHeader("Access-Control-Allow-Origin", "*");
	}

}
