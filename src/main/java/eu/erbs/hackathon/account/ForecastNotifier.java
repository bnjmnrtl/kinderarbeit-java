package eu.erbs.hackathon.account;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.erbs.hackathon.NicosTest;
import io.swagger.client.ApiException;

/**
 * Servlet implementation class AccountNotifier
 */
@WebServlet("/forecast")
public class ForecastNotifier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = LoggerFactory.getLogger(ForecastNotifier.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForecastNotifier() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		response.addHeader("Access-Control-Allow-Origin", "*");
		logger.info("Received Get Request");
		for(Entry<String, String[]> entry : request.getParameterMap().entrySet()){
			System.out.println("\t" + entry.getKey() + "\t" + entry.getValue()[0]);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Received Post Request");
		for(Entry<String, String[]> entry : request.getParameterMap().entrySet()){
			System.out.println("\t" + entry.getKey() + "\t" + entry.getValue()[0]);
		}
		
		response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
	    response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
	    try {
	    	logger.info("Trying to get balance");
	    	String forecast = NicosTest.getForecast();
	    	logger.info(forecast);
			response.getWriter().write(forecast);
		} catch (ApiException e ) {
			throw new ServletException(e);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
