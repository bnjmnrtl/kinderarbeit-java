package eu.erbs.hackathon.ahoi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class TransactionFaker {

	public static void main(String[] args) {

		
		System.out.println(getTransactionsAsString());

	}
	
	public static List<InternalTransaction> getInternalTransactions(){
		List<InternalTransaction> transactions = new ArrayList<>();
		
		transactions.add(new InternalTransaction("Kevin", "Papa", "2016-11-16", "500", "Taschengeld", true));
		transactions.add(new InternalTransaction("Kevin", "Papa", "2016-11-14", "300", "Auto waschen", true));
		transactions.add(new InternalTransaction("Sparkasse", "Kevin", "2016-11-14", "1000", "Sparkasse", false));
		transactions.add(new InternalTransaction("Kevin", "Papa", "2016-11-09", "500", "Taschengeld", true));
		transactions.add(new InternalTransaction("Kevin", "Papa", "2016-11-02", "500", "Taschengeld", true));
		transactions.add(new InternalTransaction("Sparkasse", "Kevin", "2016-11-01", "1500", "Sparkasse", false));
		transactions.add(new InternalTransaction("Kevin", "Papa", "2016-10-26", "500", "Taschengeld", true));
		
//		System.out.println(getTransaction("creditor", "payee", "date", "amount", "purpose"));
		return transactions;
	}
	
	public static String getTransactionsAsString(){
		List<String> transactionStrings = new ArrayList<>();
		for(InternalTransaction transaction : getInternalTransactions()){
			transactionStrings.add(transaction.toAppString());
		}
		return StringUtils.join(transactionStrings,"@@@");
	}

	public static String getTransactionsForAhoi(){
		List<String> transactionStrings = new ArrayList<>();
		for(InternalTransaction transaction : getInternalTransactions()){
			transactionStrings.add(transaction.toAhoiString());
		}
		return StringUtils.join(transactionStrings,"\n\n");
	}

}
