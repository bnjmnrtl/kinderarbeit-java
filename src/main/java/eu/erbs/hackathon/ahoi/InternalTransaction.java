package eu.erbs.hackathon.ahoi;

import com.google.gson.JsonObject;

public class InternalTransaction {

	private String remitter;
	private String payee;
	private String date;
	private String amount;
	private String purpose;
	private boolean incoming;

	public InternalTransaction(String remitter, String payee, String date, String amount, String purpose, boolean incoming){
		this.remitter = remitter;
		this.payee = payee;
		this.date = date;
		this.amount = amount;
		this.purpose = purpose;
		this.incoming = incoming;
	}

	public String toAhoiString() {
		//INSERT INTO `transaction` (`accountId`, `addInformation`, `amountCurrency`, `amountValue`,
		//		`availabilityDate`, `bookingDate`, `camtTxId`, `chargesOtherCurrency`, `chargesOtherValue`,
		//		`chargesOwnCurrency`, `chargesOwnValue`, `chequeNumber`, `creditorInstructedAmountCurrency`,
		//		`creditorInstructedAmountValue`, `customerRef`, `endToEndRef`, `initiatingParty`, `instrId`,
		//		`mandateRef`, `obligeeId`, `payee`, `payeeAccountNumber`, `payeeBankCode`, `payeeDivergent`,
		//		`preBooked`, `primanota`, `purpose`, `purposeCode`, `remitter`, `remitterAccountNumber`,
		//		`remitterBankCode`, `remitterDivergent`, `returnInformation`, `swiftTransactionCode`,
		//		`textKeyExtension`, `transactionType`)
		//		VALUES (161, 'KARTENZAHLUNG', 'EUR', -4428, '2016-08-24', '2016-08-24', NULL, NULL, NULL, NULL,
		//				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FRISCHECENTER MEYER//HAMBURG/DE',
		//				'DE56200907004331788071', 'EDEKDEHHXXX', NULL, b'0', '9248', '2016-08-23T17:47:08 0 2019-02',
		//				NULL, 'Nicolai Erbs', 'DE0099994000013758204', 'TESTBICXXXX', NULL, NULL, NULL, NULL, 'KARTENZAHLUNG\r');
		return "INSERT INTO `transaction` (`accountId`, `addInformation`, `amountCurrency`, `amountValue`,"
		+ " `availabilityDate`, `bookingDate`, `camtTxId`, `chargesOtherCurrency`, `chargesOtherValue`,"
		+ " `chargesOwnCurrency`, `chargesOwnValue`, `chequeNumber`, `creditorInstructedAmountCurrency`,"
		+ " `creditorInstructedAmountValue`, `customerRef`, `endToEndRef`, `initiatingParty`, `instrId`,"
		+ " `mandateRef`, `obligeeId`, `payee`, `payeeAccountNumber`, `payeeBankCode`, `payeeDivergent`,"
		+ " `preBooked`, `primanota`, `purpose`, `purposeCode`, `remitter`, `remitterAccountNumber`,"
		+ " `remitterBankCode`, `remitterDivergent`, `returnInformation`, `swiftTransactionCode`,"
		+ " `textKeyExtension`, `transactionType`) "
		+ "VALUES (161, 'KARTENZAHLUNG', 'EUR', "+amount+", '"+date+"', '"+date+"', NULL, NULL, NULL,"
		+ " NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '"+payee+"',"
		+ " 'DE56200907004331788071', 'EDEKDEHHXXX', NULL, b'0', '9248', '"+purpose+"', NULL,"
		+ " '"+remitter+"', 'DE0099994000013758204', 'TESTBICXXXX', NULL, NULL, NULL, NULL, 'KARTENZAHLUNG');";
	}

	public String toAppString(){
		String partner;
		String color;
		String signum;
		if(incoming){
			partner = payee;
			color = "green";
			signum = "";
		}
		else {
			partner = remitter;
			color = "red";
			signum = "-";
		}
		String month = date.substring(date.length()-5,date.length()-3);
		if(month.equals("11")){
			month = "November";
		}
		else{
			month = "Oktober";
		}
		return partner + ";" + purpose + ";" + signum + amount.substring(0, amount.length()-2) + ",00;" + date.substring(date.length()-2) + ". " + month + ";"  + color;

	}
}